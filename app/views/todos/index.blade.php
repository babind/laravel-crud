  @extends('layouts.main')
 @section('content')
	<h2> Show All the lists</h2>
	 <ul>
	 @foreach($todo_lists as $list)
	<h4> {{link_to('todos.show',$list->name,[$list->id] )}}</h4>
	<ul class="no-bullet button-group">
	<li>
	{{link_to_route('todos.edit','edit',[$list->id],['class'=>'tiny button'])}}
	</li>
	<li>
	{{Form::model($list,['route'=>['todos.destroy',$list->id],'method'=>'delete'])}}
	{{Form::button('delete',['type'=>'submit','class'=>'tiny alert button'])}}
	{{Form::close()}}
	</li>
	</ul>
	</ul> 
	 @endforeach
	  {{link_to_route('todos.create','create New List',null,['class'=>'success button'] )}}
	 @stop
       