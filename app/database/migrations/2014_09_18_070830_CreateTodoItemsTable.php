<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoItemsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		
		Schema::create('todo_items',function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('content')->unique();
			$table->dateTime('completed_on')->nullable();
			$table->timestamps();




			});
	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('todo_items');
	}

}
